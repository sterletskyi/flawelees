import React from "react";

import "./index.scss";

const Promo = ({ promoUrl }) => (
  <div className="promo">
    <img src={promoUrl} alt={"Promo"} />
  </div>
);

export default Promo;
