import React, { useState } from "react";

import "./index.scss";

import { BASE_COLLECTION_IMG_PATH } from "../../constants/image";

const Item = ({ img, hoverImg, title, price }) => {
  const [isHovered, setIsHovered] = useState(false);
  const imgUrl = BASE_COLLECTION_IMG_PATH + img;
  const hoverImgUrl = BASE_COLLECTION_IMG_PATH + hoverImg;

  const bgImgObj = isHovered
    ? { backgroundImage: `url(${hoverImgUrl})` }
    : { backgroundImage: `url(${imgUrl})` };
  return (
    <div
      className="item"
      onMouseLeave={() => setIsHovered(false)}
      onMouseOver={() => setIsHovered(true)}
    >
      {/* <img
        className="item-img"
        src={BASE_COLLECTION_IMG_PATH + img}
        alt={title}
      /> */}
      <div className="item-img" style={bgImgObj}></div>
      <div className="item-meta">
        <div>{title}</div>
        <div>{price}$</div>
      </div>
    </div>
  );
};

export default Item;
