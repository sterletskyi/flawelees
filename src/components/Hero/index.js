import React from "react";

import "./index.scss";

const Hero = ({ text, imgUrl }) => (
  <div className="hero">
    <div className="paralax-container">
      <div
        className="paralax-image"
        style={{ backgroundImage: `url(${imgUrl})` }}
      ></div>
      <div className="hero-content">{text}</div>
    </div>
  </div>
);

export default Hero;
