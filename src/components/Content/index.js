import React from "react";

import Filters from "../Filters";
import Grid from "../Grid";

const Content = () => {
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-3">
          <Filters />
        </div>
        <div className="col-md-9">
          <Grid />
        </div>
      </div>
    </div>
  );
};

export default Content;
