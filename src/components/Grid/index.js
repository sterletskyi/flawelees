import React from "react";

import "./index.scss";
import promo from "../../assets/floral-separator.jpg";

import Banner from "../Banner";
import Promo from "../Promo";
import Collection from "../Collection";

const Grid = () => {
  return (
    <div className="grid">
      <Banner />
      <Promo promoUrl={promo} />
      <Collection />
    </div>
  );
};

export default Grid;
