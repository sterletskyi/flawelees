import React from "react";

import "./index.scss";

import Item from "../Item";

import data from "../../data.json";

const Collection = () => {
  const messages = [...data.collection];
  return (
    <div className="collection">
      <div className="collection-header">
        <div className="collection-header--total">88 products</div>
        <div className="collection-header--sort">
          <select>
            <option>Sort</option>
            <option>Best selling</option>
            <option selected>Featured</option>
          </select>
        </div>
      </div>
      <div className="collection-content">
        {messages.map((message, index) => (
          <Item
            key={`messages-${index}`}
            img={message.img}
            hoverImg={message.hoverImg}
            title={message.title}
            price={message.price}
          />
        ))}
      </div>
    </div>
  );
};

export default Collection;
