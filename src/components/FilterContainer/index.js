import React from "react";

import Item from "../FilterItem/index";
import { colors } from "../../constants/image";

const FilterContainer = () => {
  const sleeveLength = [
    "Long Sleeve",
    "3/4 Sleeve",
    "Half Sleeve",
    "Short Sleeve",
    "Sleeveless",
  ];
  const seasons = ["spring", "fall"];
  const types = ["Tops", "Tunics", "Dresses", "Sweatshirt", "Jackets"];
  const fit = ["Relaxed", "Oversized"];

  return (
    <div className="filters">
      <Item title={"color"} items={colors} />
      <Item title={"Sleeve length"} type={"checkbox"} items={sleeveLength} />
      <Item title={"Season"} type={"checkbox"} items={seasons} />
      <Item title={"Type"} type={"checkbox"} items={types} />
      <Item title={"Fit"} type={"checkbox"} items={fit} />
    </div>
  );
};

export default FilterContainer;
