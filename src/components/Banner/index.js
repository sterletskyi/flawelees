import React from "react";

import "./index.scss";

const Banner = () => (
  <div className="banner">
    <span className="banner-title">Summer saving</span>
    <span className="banner-text">
      Save an additional 10% on all clearance items
    </span>
    <span className="banner-button">
      <p>Shop Now</p>
    </span>
  </div>
);

export default Banner;
