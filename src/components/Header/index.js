import React from "react";

import "./index.scss";

import Search from "../Icons/Search";
import Logo from "../Icons/Logo";
import Person from "../Icons/Person";
import Bag from "../Icons/Bag";

const Header = () => {
  return (
    <header className="header">
      <div className="header-item">
        <div className="site-nav--icons">
          <a href={"#"} className="site-nav--icons-item">
            <Search />
          </a>
        </div>
      </div>
      <div className="header-item w-100">
        <div className="header-item--navigation">
          <div className="header-item--navigation-item header-item--split-left">
            <ul className="site-nav">
              <li className="site-nav--link">
                <a href={"#"} className={"site-nav--link--underline"}>
                  Shop
                </a>
              </li>
              <li className="site-nav--link">
                <a href={"#"} className={"site-nav--link--underline"}>
                  Season
                </a>
              </li>
            </ul>
          </div>
          <div className="header-item--navigation-item">
            <Logo />
          </div>
          <div className="header-item--navigation-item header-item--split-right">
            <ul className="site-nav">
              <li className="site-nav--link">
                <a href={"#"} className={"site-nav--link--underline"}>
                  Journal
                </a>
              </li>
              <li className="site-nav--link">
                <a href={"#"} className={" site-nav--link--underline"}>
                  Theme Features
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="header-item">
        <div className="site-nav--icons">
          <a href={"#"} className="site-nav--icons-item">
            <Person />
          </a>
          <a href={"#"} className="site-nav--icons-item">
            <Bag />
          </a>
        </div>
      </div>
    </header>
  );
};

export default Header;
