export const BASE_COLLECTION_IMG_PATH = "/images/collection/";
export const BASE_COLOR_IMG_PATH = "/images/collection/";

export const colors = [
  `${BASE_COLOR_IMG_PATH}army_50x.png`,
  `${BASE_COLOR_IMG_PATH}black_50x.png`,
  `${BASE_COLOR_IMG_PATH}burgundy_50x.png`,
  `${BASE_COLOR_IMG_PATH}dark-gray_50x.png`,
  `${BASE_COLOR_IMG_PATH}gold_50x.png`,
  `${BASE_COLOR_IMG_PATH}heather-gray_50x.png`,
  `${BASE_COLOR_IMG_PATH}light-olive_50x.png`,
  `${BASE_COLOR_IMG_PATH}light-purple_50x.png`,
  `${BASE_COLOR_IMG_PATH}mocha_50x.png`,
  `${BASE_COLOR_IMG_PATH}resewood_50x.png`,
];
