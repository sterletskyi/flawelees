import React from "react";

import Header from "../../components/Header";
import Hero from "../../components/Hero";
import Content from "../../components/Content";

import hero from "../../assets/hero.jpg";

import "./index.scss";

const App = () => {
  return (
    <div className="App">
      <Header />
      <Hero text={"Spring"} imgUrl={hero} />
      <Content />
    </div>
  );
};

export default App;
